# B2 Réseau 2022 - TP1

# TP1 - Mise en jambes

**Dans ce TP, on va explorer le réseau de vos clients, vos PC.**

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

- nom, [adresse MAC](../../cours/lexique.md#mac-media-access-control) et adresse IP de l'interface WiFi
```
PS C:\Users\Thomas> ipconfig /all
Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
Adresse physique . . . . . . . . . . . : FC-B3-BC-93-06-CA
Adresse IPv4. . . . . . . . . . . . . .: 10.33.18.87

```
- nom, [adresse MAC](../../cours/lexique.md#mac-media-access-control) et adresse IP de l'interface Ethernet
```
PS C:\Users\Thomas> ipconfig /all
Description. . . . . . . . . . . . . . : Bluetooth Device (Personal Area Network)
Adresse physique . . . . . . . . . . . : FC-B3-BC-93-06-CE
```

**🌞 Affichez votre gateway**

- utilisez une commande pour connaître l'adresse IP de la [passerelle](../../cours/lexique.md#passerelle-ou-gateway) de votre carte WiFi
```
PS C:\Users\Thomas> ipconfig /all
Passerelle par défaut. . . . . . . . . : 10.33.19.254
```

### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

- trouvez l'IP, la MAC et la [gateway](../../cours/lexique.md#passerelle-ou-gateway) pour l'interface WiFi de votre PC
Aller dans les paramètres sur windows, aller dans "Réseau et Internet", Aller dans la catégorie "état" puis dans "Afficher les propriétés du matériel et de la connexion" puis chercher la carte Wifi et les différents informations comme ci-dessous s'affichent.
```
Nom :   Wifi
Description :   Intel(R) Wi-Fi 6 AX201 160MHz
Adresse physique (MAC) :   FC-B3-BC-93-06-CA
Adresse IPv4 :   10.33.18.87/22
Passerelle par défaut :   10.33.19.254
```

### Questions

- 🌞 à quoi sert la [gateway](../../cours/lexique.md#passerelle-ou-gateway) dans le réseau d'YNOV ?

Elle sert à passer du réseau LAN au réseau WAN ( réseau privé au réseau publique ).


## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

- changez l'adresse IP de votre carte WiFi pour une autre
- ne changez que le dernier octet
  - par exemple pour `10.33.1.10`, ne changez que le `10`
  - valeur entre 1 et 254 compris

![image IP](./pictures.png)

🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

On perd l'accès à Internet car l'adresse IP que l'on prend est surement déjà utilisé par un autre utilisateur du réseau, donc lorsque deux personnes ont la même adresse IP, les paquets ne savent pas où aller et ca bug.


# II. Exploration locale en duo

Owkay. Vous savez à ce stade :

- afficher les informations IP de votre machine
- modifier les informations IP de votre machine
- c'est un premier pas vers la maîtrise de votre outil de travail

On va maintenant répéter un peu ces opérations, mais en créant un réseau local de toutes pièces : entre deux PCs connectés avec un câble RJ45.

## 1. Prérequis

- deux PCs avec ports RJ45 ( je n'ai pas de port RJ45 sur mon PC je vais donc suivre avec Mehdi et Matthias)
- un câble RJ45
- **firewalls désactivés** sur les deux PCs

## 2. Câblage

Ok c'est la partie tendue. Prenez un câble. Branchez-le des deux côtés. **Bap.**

## 3. Modification d'adresse IP

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau
  - choisissez une IP qui commence par "192.168"
  - utilisez un /30 (que deux IP disponibles)
- vérifiez à l'aide de commandes que vos changements ont pris effet

![changement IP](./ipconfig.png)

- utilisez `ping` pour tester la connectivité entre les deux machines

![ping](./ping.png)

- affichez et consultez votre table ARP

![ARP](./ARP.png)

# 4. Utilisation d'un des deux comme gateway

Ca, ça peut toujours dépann. Comme pour donner internet à une tour sans WiFi quand y'a un PC portable à côté, par exemple.

- 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu
  - encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait

![pingethernet](./pingEthernet.png)

- 🌞 utiliser un `traceroute` ou `tracert` pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

![tracert](./tracert.png)

## 5. Petit chat privé

On va créer un chat extrêmement simpliste à l'aide de `netcat` (abrégé `nc`). Il est souvent considéré comme un bon couteau-suisse quand il s'agit de faire des choses avec le réseau.

Here we go :

- 🌞 **sur le PC *serveur*** avec par exemple l'IP 192.168.1.1
  - `nc.exe -l -p 8888`
    - "`netcat`, écoute sur le port numéro 8888 stp"
  - il se passe rien ? Normal, faut attendre qu'un client se connecte
- 🌞 **sur le PC *client*** avec par exemple l'IP 192.168.1.2
  - `nc.exe 192.168.1.1 8888`
    - "`netcat`, connecte toi au port 8888 de la machine 192.168.1.1 stp"
  - une fois fait, vous pouvez taper des messages dans les deux sens

![chat](./chat.png)

- 🌞 pour aller un peu plus loin
  - le serveur peut préciser sur quelle IP écouter, et ne pas répondre sur les autres
  - par exemple, on écoute sur l'interface Ethernet, mais pas sur la WiFI
  - pour ce faire `nc.exe -l -p PORT_NUMBER IP_ADDRESS`
  - par exemple `nc.exe -l -p 9999 192.168.1.37`
  - on peut aussi accepter uniquement les connexions internes à la machine en écoutant sur `127.0.0.1`

![chatv2](./chatV2.png)

## 6. Firewall

Toujours par 2.

Le but est de configurer votre firewall plutôt que de le désactiver

- Activez votre firewall
- 🌞 Autoriser les `ping`
  - configurer le firewall de votre OS pour accepter le `ping`
  - aidez vous d'internet
  - on rentrera dans l'explication dans un prochain cours mais sachez que `ping` envoie un message *ICMP de type 8* (demande d'ECHO) et reçoit un message *ICMP de type 0* (réponse d'écho) en retour

![firewallPing](./FireWallPing.png)

- 🌞 Autoriser le traffic sur le port qu'utilise `nc`
  - on parle bien d'ouverture de **port** TCP et/ou UDP
  - on ne parle **PAS** d'autoriser le programme `nc`
  - choisissez arbitrairement un port entre 1024 et 20000
  - vous utiliserez ce port pour [communiquer avec `netcat`](#5-petit-chat-privé-) par groupe de 2 toujours
  - le firewall du *PC serveur* devra avoir un firewall activé et un `netcat` qui fonctionne

![firewallNC](./FireWallNC.png)

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

Bon ok vous savez définir des IPs à la main. Mais pour être dans le réseau YNOV, vous l'avez jamais fait.  

C'est le **serveur DHCP** d'YNOV qui vous a donné une IP.

Une fois que le serveur DHCP vous a donné une IP, vous enregistrer un fichier appelé *bail DHCP* qui contient, entre autres :

- l'IP qu'on vous a donné
- le réseau dans lequel cette IP est valable

🌞Exploration du DHCP, depuis votre PC

- afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
- cette adresse a une durée de vie limitée. C'est le principe du ***bail DHCP*** (ou *DHCP lease*). Trouver la date d'expiration de votre bail DHCP

![DHCP](./DHCP.png)

- vous pouvez vous renseigner un peu sur le fonctionnement de DHCP dans les grandes lignes. On aura sûrement un cours là dessus :)


## 2. DNS

Le protocole DNS permet la résolution de noms de domaine vers des adresses IP. Ce protocole permet d'aller sur `google.com` plutôt que de devoir connaître et utiliser l'adresse IP du serveur de Google.

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
PS C:\Users\Thomas> ipconfig /all
Carte réseau sans fil Wi-Fi :

Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
```

- 🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

  - faites un *lookup* (*lookup* = "dis moi à quelle IP se trouve tel nom de domaine")
    - pour `google.com`
```
    PS C:\Users\Thomas> nslookup google.com
    Serveur :   dns.google
    Address:  8.8.8.8

    Réponse ne faisant pas autorité :
    Nom :    google.com
    Addresses:  2a00:1450:4007:808::200e
              216.58.215.46
```

    - pour `ynov.com`
```
    PS C:\Users\Thomas> nslookup ynov.com
    Serveur :   dns.google
    Address:  8.8.8.8

    Réponse ne faisant pas autorité :
    Nom :    ynov.com
    Addresses:  2606:4700:20::681a:ae9
              2606:4700:20::681a:be9
              2606:4700:20::ac43:4ae2
              172.67.74.226
              104.26.10.233
              104.26.11.233
```

    - interpréter les résultats de ces commandes
    La commande qu'on vient de faire nous montre à quelles IP le sites qu'on demande est relié par exemple pour le site google.com, qui a pour adresse 8.8.8.8, réponds à l'adresse IPV4 : 216.58.215.46 .
    On peut aussi voir au niveau du nslookup de ynov.com qu'il y a plusieurs adresses IPV4 qui correspondent au site, ce qui montre qu'il y a de la répartition de charge au niveau du DNS.
  - déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
  - faites un *reverse lookup* (= "dis moi si tu connais un nom de domaine pour telle IP")
    - pour l'adresse `78.74.21.21`
```
    PS C:\Users\Thomas> nslookup 78.74.21.21
    Serveur :   dns.google
    Address:  8.8.8.8

    Nom :    host-78-74-21-21.homerun.telia.com
    Address:  78.74.21.21
```

    - pour l'adresse `92.146.54.88`
```
    PS C:\Users\Thomas> nslookup 92.146.54.88
Serveur :   dns.google
Address:  8.8.8.8

*** dns.google ne parvient pas à trouver 92.146.54.88 : Non-existent domain
```
    - interpréter les résultats
    On peut voir avec ce reverse lookup que l'adresse IP 78.74.21.21 correspond au site internet host-78-74-21-21.homerun.telia.com, et que l'adresse ip 92.146.54.88 ne correspond elle a aucun site internet.

    - *si vous vous demandez, j'ai pris des adresses random :)*


# IV. Wireshark

Wireshark est un outil qui permet de visualiser toutes les trames qui sortent et entrent d'une carte réseau.

Il peut :

- enregistrer le trafic réseau, pour l'analyser plus tard
- afficher le trafic réseau en temps réel

- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
  - un `ping` entre vous et la passerelle

  ![pingPasserelle](./pingPasserelle.png)

  - un `netcat` entre vous et votre mate, branché en RJ45

  je n'ai malheureusement pas de PC avec un port RJ45 et je n'ai pas eu le temps de le faire avec mes mates donc je ne l'ai pas.

  - une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.

  ![wiresharkDNS](./wiresharkDNS.png)
