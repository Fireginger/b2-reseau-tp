# TP4 : TCP, UDP et services réseau

Dans ce TP on va explorer un peu les protocoles TCP et UDP. On va aussi mettre en place des services qui font appel à ces protocoles.

# 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux.

# I. First steps

Faites-vous un petit top 5 des applications que vous utilisez sur votre PC souvent, des applications qui utilisent le réseau : un site que vous visitez souvent, un jeu en ligne, Spotify, j'sais po moi, n'importe.

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

- avec Wireshark, on va faire les chirurgiens réseau
- déterminez, pour chaque application :
  - IP et port du serveur auquel vous vous connectez
  - le port local que vous ouvrez pour vous connecter

![lancementGoogle](./lancement_google.pcapng)
IP du serveur 8.8.8.8 et port 1900
port local ouvert 53771

![lancementLOL](./lol_lancement.pcapng)
IP du serveur 35.155.187.112 et port 443
port local ouvert 25031

![fermetureLOL](./lol_fin.pcapng)
IP du serveur 35.155.187.112 et port 443
port local ouvert 25031

![lancementOutlook](./lancement_outlook.pcapng)
IP du serveur 52.111.231.0 et port 443
port local ouvert 1171

![vidéoYoutube](./youtube_vidéo.pcapng)
MAC du serveur 2a00:1450:4007:2a::8 et port 443
port local ouvert 52594

🌞 **Demandez l'avis à votre OS**

- votre OS est responsable de l'ouverture des ports, et de placer un programme en "écoute" sur un port
- il est aussi responsable de l'ouverture d'un port quand une application demande à se connecter à distance vers un serveur
- bref il voit tout quoi
- utilisez la commande adaptée à votre OS pour repérer, dans la liste de toutes les connexions réseau établies, la connexion que vous voyez dans Wireshark, pour chacune des 5 applications

```
pour le lancement de google :  TCP    192.168.1.16:1024      162.159.130.234:https  ESTABLISHED
```

```
pour la fermeture de LOL je n'arrive pas a voir
```

```
pour le lancement de LOL :  TCP    192.168.1.16:1036      20.43.44.165:8883      ESTABLISHED
```

```
pour le lancement de outlook : TCP    192.168.1.16:24306     52.111.231.13:443      ESTABLISHED
```

```
pour la vidéo youtube :  TCP    192.168.1.16:24223     195.122.177.172:https  ESTABLISHED
```

**Il faudra ajouter des options adaptées aux commandes pour y voir clair. Pour rappel, vous cherchez des connexions TCP ou UDP.**

# II. Mise en place

Allumez une VM Linux pour la suite.

## 1. SSH

Connectez-vous en SSH à votre VM.

🌞 **Examinez le trafic dans Wireshark**

- donnez un sens aux infos devant vos yeux, capturez un peu de trafic, et coupez la capture, sélectionnez une trame random et regardez dedans, vous laissez pas brainfuck par Wireshark n_n
- **déterminez si SSH utilise TCP ou UDP**
  - pareil réfléchissez-y deux minutes, logique qu'on utilise pas UDP non ?
- **repérez le *3-Way Handshake* à l'établissement de la connexion**
  - c'est le `SYN` `SYNACK` `ACK`
- **repérez le FIN FINACK à la fin d'une connexion**
- entre le *3-way handshake* et l'échange `FIN`, c'est juste une bouillie de caca chiffré, dans un tunnel TCP

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion SSH depuis votre machine
- ET repérez la connexion SSH depuis votre VM

Depuis la VM :
```
[thomas@localhost ~]$ ss -npaet
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN          0               128                            0.0.0.0:22                          0.0.0.0:*             ino:18286 sk:2c cgroup:/system.slice/sshd.service <->
```

Depuis ma machine :
```
PS C:\Users\Thomas> netstat

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    10.3.1.1:1086          10.3.1.11:ssh          ESTABLISHED
```

🦈 **Je veux une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

![capturessh](./ssh_capture.pcapng)

## 2. NFS

Allumez une deuxième VM Linux pour cette partie.

Vous allez installer un serveur NFS. Un serveur NFS c'est juste un programme qui écoute sur un port (comme toujours en fait, oèoèoè) et qui propose aux clients d'accéder à des dossiers à travers le réseau.

Une de vos VMs portera donc le serveur NFS, et l'autre utilisera un dossier à travers le réseau.

🌞 **Mettez en place un petit serveur NFS sur l'une des deux VMs**

- j'vais pas ré-écrire la roue, google it, ou [go ici](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=1)
- partagez un dossier que vous avez créé au préalable dans `/srv`
- vérifiez que vous accédez à ce dossier avec l'autre machine : [le client NFS](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)

Je ne comprends pas lordque j'essaye de mount sur le client ca tourne a l infini donc je ne peux pas avancer.
