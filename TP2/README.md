# TP2 : Ethernet, IP, et ARP

Dans ce TP on va approfondir trois protocoles, qu'on a survolé jusqu'alors :

- **IPv4** *(Internet Protocol Version 4)* : gestion des adresses IP
  - on va aussi parler d'ICMP, de DHCP, bref de tous les potes d'IP quoi !
- **Ethernet** : gestion des adresses MAC
- **ARP** *(Address Resolution Protocol)* : permet de trouver l'adresse MAC de quelqu'un sur notre réseau dont on connaît l'adresse IP

# 0. Prérequis

**Il vous faudra deux machines**, vous êtes libres :

- toujours possible de se connecter à deux avec un câble

**Toutes les manipulations devront être effectuées depuis la ligne de commande.** Donc normalement, plus de screens.


# I. Setup IP

🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**
- vous renseignerez dans le compte rendu :
  - les deux IPs choisies, en précisant le masque
  ```
  Adresse IPV4 choisies : 192.168.0.1 et 192.168.0.2
  Masque de sous-réseau : 255.255.255.192
  ```
  - l'adresse de réseau
  ```
  Adresse de réseau : 192.168.0.0
  ```
  - l'adresse de broadcast
  ```
  Adresse de broadcast : 192.168.0.63
  ```
- vous renseignerez aussi les commandes utilisées pour définir les adresses IP *via* la ligne de commande
```
PS C:\Windows\system32> New-NetIPAddress -InterfaceAlias "Ethernet 3" -IPAddress 192.168.0.1 -PrefixLength 26
```

> Rappel : tout doit être fait *via* la ligne de commandes. Faites-vous du bien, et utilisez Powershell plutôt que l'antique cmd sous Windows svp.

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**
```
PS C:\Windows\system32> ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```

🌞 **Wireshark it**

- `ping` ça envoie des paquets de type ICMP (c'est pas de l'IP, c'est un de ses frères)
  - les paquets ICMP sont encapsulés dans des trames Ethernet, comme les paquets IP
  - il existe plusieurs types de paquets ICMP, qui servent à faire des trucs différents
- **déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par `ping`**

  - pour le ping que vous envoyez :
    ***C'est une request.***
  - et le pong que vous recevez en retour :
    ***C'est une reply.***

🦈 **PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

![pingwireshark](./wiresharkPing.pcapng)


# II. ARP my bro

🌞 **Check the ARP table**

- utilisez une commande pour afficher votre table ARP
```
PS C:\Windows\system32> arp -a
```
- déterminez la MAC de votre binome depuis votre table ARP
```
Interface : 192.168.0.1 --- 0x4a
  Adresse Internet      Adresse physique      Type
  192.168.0.2           08-8f-c3-04-81-91     dynamique
```
- déterminez la MAC de la *gateway* de votre réseau 
  - celle de votre réseau physique, WiFi, genre YNOV, car il n'y en a pas dans votre ptit LAN
  ```
  Interface : 10.33.18.87 --- 0x9
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  ```
  - c'est juste pour vous faire manipuler un peu encore :)

> Il peut être utile de ré-effectuer des `ping` avant d'afficher la table ARP. En effet : les infos stockées dans la table ARP ne sont stockées que temporairement. Ce laps de temps est de l'ordre de ~60 secondes sur la plupart de nos machines.

🌞 **Manipuler la table ARP**

- utilisez une commande pour vider votre table ARP
```
PS C:\Windows\system32> arp -d
```
- prouvez que ça fonctionne en l'affichant et en constatant les changements
Avant :
```
Interface : 192.168.0.1 --- 0x4a
  Adresse Internet      Adresse physique      Type
  192.168.0.2           08-8f-c3-04-81-91     dynamique
  192.168.0.63          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
Après
```
Interface : 192.168.0.1 --- 0x4a
  Adresse Internet      Adresse physique      Type
  192.168.0.63          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
```
On peut voir que pour "192.168.0.2" et pour "239.255.255.250" ils ont disparus de la table arp, il ne reste que "224.0.0.22" et l'adresse de broadcast dans la table.

- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP
```
Interface : 192.168.0.1 --- 0x4a
  Adresse Internet      Adresse physique      Type
  192.168.0.2           08-8f-c3-04-81-91     dynamique
  192.168.0.63          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

> Les échanges ARP sont effectuées automatiquement par votre machine lorsqu'elle essaie de joindre une machine sur le même LAN qu'elle. Si la MAC du destinataire n'est pas déjà dans la table ARP, alors un échange ARP sera déclenché.

🌞 **Wireshark it**

- vous savez maintenant comment forcer un échange ARP : il sufit de vider la table ARP et tenter de contacter quelqu'un, l'échange ARP se fait automatiquement
- mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois
  - déterminez, pour les deux trames, les adresses source et destination  
    **Pour le broadcast :** *-l'adresse source c'est : Luxshare_0b:15:00 (moi).*  
                            *-la destination c'est tout le monde.*  
    **Pour la reply :** *-l'adresse source c'est : 08:8f:c3:04:81:91	(matthias)*  
                        *-la destination c'est : Luxshare_0b:15:00 (moi)*  

🦈 **PCAP qui contient les trames ARP**

![arpBroadcast](./arpBroadcast.pcapng)


# III. DHCP you too my brooo

🌞 **Wireshark it**

- identifiez les 4 trames DHCP lors d'un échange DHCP
  - mettez en évidence les adresses source et destination de chaque trame
  ```
  Discover : adresse source = fc:b3:bc:93:06:ca et destination = ff:ff:ff:ff:ff:ff
  Offer : adresse source = 00:c0:e7:e0:04:4e et destination = fc:b3:bc:93:06:ca
  Request : adresse source = fc:b3:bc:93:06:ca et destination = ff:ff:ff:ff:ff:ff
  Acknowledge : adresse source = 00:c0:e7:e0:04:4e et destination = fc:b3:bc:93:06:ca
  ```
- identifiez dans ces 4 trames les informations **1**, **2** et **3** dont on a parlé juste au dessus

```
IP à utiliser = 10.33.18.86
Adresse IP de la passerelle du réseau = 10.33.19.254
Adresse d'un serveur DNS joignable depuis ce réseau = 8.8.8.8
```

🦈 **PCAP qui contient l'échange DORA**

![wiresharkDORA](./DORA.pcapng)


# IV. Avant-goût TCP et UDP

🌞 **Wireshark it**

- déterminez à quelle IP et quel port votre PC se connecte quand vous regardez une vidéo Youtube
```
IP = 91.68.245.81	et port = 443
```
  - il sera sûrement plus simple de repérer le trafic Youtube en fermant tous les autres onglets et toutes les autres applications utilisant du réseau

🦈 **PCAP qui contient un extrait de l'échange qui vous a permis d'identifier les infos**

![TCPwireshark](./tcpwireshark.pcapng)
