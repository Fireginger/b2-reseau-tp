# TP3 : On va router des trucs

Au menu de ce TP, on va revoir un peu ARP et IP histoire de **se mettre en jambes dans un environnement avec des VMs**.

Puis on mettra en place **un routage simple, pour permettre à deux LANs de communiquer**.

## 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux.

## I. ARP

Première partie simple, on va avoir besoin de 2 VMs.

### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre
```
[thomas@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.04 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=1.02 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.899 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.885 ms
--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 0.885/0.959/1.036/0.067 ms
```
```
[thomas@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.767 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=1.01 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=1.07 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.951 ms
--- 10.3.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 0.767/0.949/1.066/0.112 ms
```
- observer les tables ARP des deux machines
```
[thomas@localhost ~]$ arp
Address       HW type         HW address            Flags Mask     Iface
10.3.1.12     ether           08:00:27:7d:85:aa     C              enp0s8
10.3.1.1      ether           0a:00:27:00:00:15     C              enp0s8
```
```
[thomas@localhost ~]$ arp
Address       HW type         HW address            Flags Mask     Iface
10.3.1.11     ether           08:00:27:ac:b3:3c     C              enp0s8
10.3.1.1      ether           0a:00:27:00:00:15     C              enp0s8
```
- repérer l'adresse MAC de `john` dans la table ARP de `marcel` et vice-versa
```
Dans la table de Marcel (10.3.1.12), l'adresse MAC de John est : 08:00:27:ac:b3:3c
```
```
Dans la table de John (10.3.1.11), l'adresse MAC de Marcel est : 08:00:27:7d:85:aa
```
- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
  - une commande pour voir la MAC de `marcel` dans la table ARP de `john`
```
[thomas@localhost ~]$ arp -a |grep 10.3.1.12
? (10.3.1.12) at 08:00:27:7d:85:aa [ether] on enp0s8
```
  - et une commande pour afficher la MAC de `marcel`, depuis `marcel`
```
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
link/ether 08:00:27:7d:85:aa
```

### 2. Analyse de trames

🌞**Analyse de trames**

- utilisez la commande `tcpdump` pour réaliser une capture de trame
```
[thomas@localhost ~]$ sudo tcpdump -i enp0s8 arp -vv -w TCPDUMP2.pcapng -c 2
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes
```
- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`
```
[thomas@localhost ~]$ sudo arp -d 10.3.1.11 et [thomas@localhost ~]$ sudo arp -d 10.3.1.12
```
🦈 **Capture réseau `tp2_arp.pcapng`** qui contient un ARP request et un ARP reply

![TCPDUMP](./TCPDUMP.pcapng)

## II. Routage

Vous aurez besoin de 3 VMs pour cette partie.

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**
```
[thomas@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[thomas@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

Ensuite on peut voir que le routage est activé :
```
[thomas@localhost ~]$ sudo firewall-cmd --list-all
masquerade: yes
``` 

🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

- il faut ajouter une seule route des deux côtés
pour john(10.3.1.11) :
```
[thomas@localhost ~]$ sudo route add -net 10.3.2.0 netmask 255.255.255.0 gw 10.3.1.254
```
pour marcel(10.3.2.12) :
```
[thomas@localhost ~]$ sudo route add -net 10.3.1.0 netmask 255.255.255.0 gw 10.3.2.254
```
- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre
```
[thomas@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.27 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.86 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.91 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=63 time=2.06 ms
--- 10.3.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 1.270/1.773/2.056/0.299 ms
```
```
[thomas@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=63 time=1.51 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=63 time=1.97 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=63 time=1.59 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=63 time=1.93 ms
--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 1.511/1.751/1.972/0.202 ms
```

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds
- effectuez un `ping` de `john` vers `marcel`
- regardez les tables ARP des trois noeuds
- essayez de déduire un peu les échanges ARP qui ont eu lieu

Chez john, l'addresse ip du routeur (10.3.1.254) s'est ajouté dans la table ARP mais c'est tout.
Chez marcel, l'adresse ip du routeur (10.3.2.254) s'est ajouté dans la table ARP mais c'est tout.
Chez le routeur, l'adresse ip des deux machines (10.3.1.11 et 10.3.2.12) se sont rajoutés.
On peut donc bien voir que les deux machines ne communiquent pas directement entres elles mais via le routeur.

- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `marcel`
```
[thomas@localhost ~]$ sudo tcpdump -i enp0s8 icmp -t -w Marcel.pcapng
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes
```

- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame  | IP source              | MAC source                  | IP destination       | MAC destination             |
|-------|-------------|------------------------|-----------------------------|----------------------|-----------------------------|
| 1     | Requête ARP | 10.3.1.11              | `john` `08:00:27:ac:b3:3`   | tout le monde        | Broadcast `FF:FF:FF:FF:FF`  |
| 2     | Réponse ARP | localhost.domain       | ?                           | 10.3.1.11            | `john` `08:00:27:ac:b3:3c`  |
| ...   | ...         | ...                    | ...                         |                      |                             |
| ?     | Ping        | 10.3.1.11              | `john` `08:00:27:ac:b3:3c`  | 10.3.2.12            | `marcel` `08:00:27:7d:85:aa`|
| ?     | Pong        | 10.3.2.12              | `marcel` `08:00:27:7d:85:aa`| 10.3.1.11            | `john` `08:00:27:ac:b3:3c`  |

> Vous pourriez, par curiosité, lancer la capture sur `john` aussi, pour voir l'échange qu'il a effectué de son côté.

🦈 **Capture réseau `tp2_routage_marcel.pcapng`**

![Marcel](./Marcel.pcapng)

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet
- ajoutez une route par défaut à `john` et `marcel`
```
[thomas@localhost ~]$ sudo ip route add default via 10.3.2.254
```
```
[thomas@localhost ~]$ sudo ip route add default via 10.3.1.254
```
  - vérifiez que vous avez accès internet avec un `ping`
  - le `ping` doit être vers une IP, PAS un nom de domaine
  ```
  [thomas@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=23.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=24.3 ms
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 22.526/23.503/24.309/0.737 ms
```
- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
```
[thomas@localhost ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search home
nameserver 1.1.1.1

curl gitlab.com
```
  - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
```
[thomas@localhost ~]$ dig q-type
;; Query time: 26 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Tue Oct 11 21:59:50 CEST 2022
;; MSG SIZE  rcvd: 110
```
  - puis avec un `ping` vers un nom de domaine
```
[thomas@localhost ~]$ ping ynov.com
PING ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=53 time=23.7 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=53 time=22.4 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=3 ttl=53 time=22.3 ms
--- ynov.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
rtt min/avg/max/mdev = 22.341/22.818/23.744/0.654 ms
```

🌞**Analyse de trames**

- effectuez un `ping 8.8.8.8` depuis `john`
```
[thomas@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=22.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=23.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=24.2 ms
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 22.141/23.297/24.191/0.857 ms
```
- capturez le ping depuis `john` avec `tcpdump`
- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source          | MAC source                 | IP destination    | MAC destination   |     |
|-------|------------|--------------------|----------------------------|-------------------|-------------------|-----|
| 1     | ping       | `john` `10.3.1.11` | `john` `08:00:27:ac:b3:3c` | `8.8.8.8`         | 08:00:27:09:39:e4 |     |
| 2     | pong       | `8.8.8.8`          | 08:00:27:09:39:e4          | `john` `10.3.1.11`| 08:00:27:ac:b3:3c | ... |

🦈 **Capture réseau `tp2_routage_internet.pcapng`**

Ethernet II, Src: PcsCompu_09:39:e4 (08:00:27:09:39:e4), Dst: PcsCompu_ac:b3:3c (08:00:27:ac:b3:3c)
![Google](./Google.pcapng)

## III. DHCP

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `john`
```
[thomas@localhost ~]$ sudo dnf install dhcp-server
```
Fichier de conf :
```
[thomas@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 10.3.1.0 netmask 255.255.255.0 {
  range 10.3.1.50 10.3.1.250;
  option broadcast-address 10.3.1.255;
}
```
```
[thomas@localhost ~]$ sudo firewall-cmd --permanent --add-port=67/udp
[thomas@localhost ~]$ sudo systemctl enable --now dhcpd
```

- faites lui récupérer une IP en DHCP à l'aide de votre serveur
```
[thomas@localhost ~]$ sudo dhclient
```
On peut voir si l'on fait ip -a que Bob a maintenant une adresse IP :
```
[thomas@localhost ~]$ ip a |grep enp0s8
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s8
```

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
```
[thomas@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 10.3.1.0 netmask 255.255.255.0 {
  range 10.3.1.50 10.3.1.250;
  option broadcast-address 10.3.1.255;

  option routers 10.3.1.254;
  option domain-name-servers 8.8.8.8;
}
```
- récupérez de nouveau une IP en DHCP sur `bob` pour tester :
```
[thomas@localhost ~]$ sudo dhclient -r
[thomas@localhost ~]$ sudo dhclient
[thomas@localhost ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:30:72:5c brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 740sec preferred_lft 740sec
    inet 10.3.1.51/24 brd 10.3.1.255 scope global secondary dynamic enp0s8
       valid_lft 939sec preferred_lft 939sec
    inet6 fe80::a00:27ff:fe30:725c/64 scope link
       valid_lft forever preferred_lft forever
```
  - `marcel` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP

    - vérifier qu'il peut `ping` sa passerelle
```
    [thomas@localhost ~]$ ping 10.3.1.254
    PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
    64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.718 ms
    64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=0.902 ms
    64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=0.919 ms
    --- 10.3.1.254 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2035ms
    rtt min/avg/max/mdev = 0.718/0.846/0.919/0.091 ms
```
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande
```
    [thomas@localhost ~]$ ip route show
    default via 10.3.1.254 dev enp0s8
    10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.50 metric 100
```

    - vérifier que la route fonctionne avec un `ping` vers une IP

```
[thomas@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.89 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.82 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.83 ms
--- 10.3.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 1.824/1.848/1.889/0.028 ms
```
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne

```
[thomas@localhost ~]$ dig ynov.com
    ;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               300     IN      A       104.26.11.233
ynov.com.               300     IN      A       172.67.74.226
ynov.com.               300     IN      A       104.26.10.233
```
    
    - vérifier un `ping` vers un nom de domaine

```
[thomas@localhost ~]$ ping ynov.com
PING ynov.com (104.26.11.233) 56(84) bytes of data.
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=1 ttl=52 time=18.6 ms
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=2 ttl=52 time=17.4 ms
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=3 ttl=52 time=17.9 ms
--- ynov.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 17.381/17.985/18.638/0.514 ms
```
## 2. Analyse de trames

🌞**Analyse de trames**

- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
- demander une nouvelle IP afin de générer un échange DHCP
- exportez le fichier `.pcapng`

![TramesDHCP](./Trames.pcapng)
